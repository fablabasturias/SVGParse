import processing.serial.*;

// CONFIGURATION
int WIDTH = 600;
int HEIGHT = 600;

// G-Code
int NORMALSPEED = 1000;
int JUMPSPEED = 5000;
int PRINT_HEIGHT = 1;
boolean GO_UP_WHEN_JUMPING = false;
int GO_UP_DISTANCE = 2;
boolean CONCENTRIC_CIRCLES = true;

String header = 
"G90\n" +                             // absolute positioning
"G21\n" +                             // millimeters
"G1 Z40 F5000\n" +                    // move Z up 
"G28\n" +                             // home
"G1 Z40 F5000\n" +                    // move Z up again
"G1 X100 Y100 F5000\n" +              // move to center
"G1 Z" + PRINT_HEIGHT + " F5000\n";   // move Z to print height

String footer = 
"G1 Z40 F5000\n" +                    // move Z up
"G28 X0 Y0\n" +                       // home X and Y
"M84\n" +                             // stop motors
"M104 S0\n";                          // turn off heat


// Globals
Boolean fileOpened = false;
Button loadFileButton;
Button loadBacteriaButton;
Button printButton;
XML svgcode;
int currentLine;

ArrayList<Circle> myCircles = new ArrayList<Circle>();
IntDict colorClasses;

int numberOfColors = 0;

// current operation
int processCounter = 0;
int currentGcode = 0;

ArrayList<String> gcodes;

// Serial
Serial port = null;
String portname = null;
Boolean portOpen = false;

String[] gcodeOutput;
int currentGcodeLine = 0;
Boolean sending = false;


// Classes
class Button {
    int x, y, w, h;
    String btext;
    color bfill;
    color bstroke;

    Button(int nx, int ny, int nw, int nh, color nfill, color nstroke, String ntext) {
        x=nx;
        y=ny;
        w=nw;
        h=nh;
        btext=ntext;
        bfill=nfill;
        bstroke=nstroke;
    }

    Button(int nx, int ny,int nw, int nh, String ntext) {
        x=nx;
        y=ny;
        w=nw;
        h=nh;
        btext=ntext;
        bfill=0;
        bstroke=255;
    }

    void show() {
        fill(bfill);
        stroke(bstroke);
        rect(x,y,w,h);
        textSize(12);
        textAlign(CENTER);
        fill(bstroke);
        text(btext,x+(w/2), (y+(h/2))+12/2);
    }

    boolean overButton()  {
        if (mouseX >= x && mouseX <= x+w && 
                mouseY >= y && mouseY <= y+h) {
            return true;
        } else {
            return false;
        }
    }
}


class Circle {
    float x, y, radius;
    int cl;             // color

    Circle(float nx, float ny, float nradius, int nc) {
        x = nx;
        y = ny;
        radius = nradius;
        cl = nc;
    }
}


// SETUP //////////////////////////////

void setup() {
    size(WIDTH,HEIGHT);

    // color codes
    colorClasses = new IntDict(); 

    colorClasses.set("st0", 0xfffbf236);
    colorClasses.set("st1", 0xff99e550);
    colorClasses.set("st2", 0xff6abe30);
    colorClasses.set("st3", 0xff37946e);
    colorClasses.set("st4", 0xff4b692f);
    colorClasses.set("st5", 0xff524b24);
    colorClasses.set("st6", 0xff323c39);
    colorClasses.set("st7", 0xff3f3f74);

    colorClasses.set("st8", 0xff222222);
    colorClasses.set("st9", 0xff222034);
    colorClasses.set("st10", 0xff45283c);
    colorClasses.set("st11", 0xff663931);
    colorClasses.set("st12", 0xff8f563b);
    colorClasses.set("st13", 0xffdf7126);
    colorClasses.set("st14", 0xffd9a066);
    colorClasses.set("st15", 0xffeec39a);

    colorClasses.set("st16", 0xff306082);
    colorClasses.set("st17", 0xff5b6ee1);
    colorClasses.set("st18", 0xff639bff);
    colorClasses.set("st19", 0xff5fcde4);
    colorClasses.set("st20", 0xffcbdbfc);
    colorClasses.set("st21", 0xffffffff);
    colorClasses.set("st22", 0xff9badb7);
    colorClasses.set("st23", 0xff847e87);

    colorClasses.set("st24", 0xff696a6a);
    colorClasses.set("st25", 0xff595652);
    colorClasses.set("st26", 0xff76428a);
    colorClasses.set("st27", 0xffac3232);
    colorClasses.set("st28", 0xffd95763);
    colorClasses.set("st29", 0xffd777b2);
    colorClasses.set("st30", 0xff8f974a);
    colorClasses.set("st31", 0xff8a6f30);

    smooth();

    loadFileButton= new Button((WIDTH/2)-100/2, (HEIGHT/2)-50/2, 100,50, "Load File");
    loadBacteriaButton = new Button(WIDTH-110, 10, 100, 50, "");
    printButton = new Button(WIDTH-110, 10, 100, 50, "");

    // serial
    // port = new Serial(this, portname, 115200);
    portOpen = true;
}

// DRAW ///////////////////////////////

void draw() {
    background(0);

    switch (processCounter) {
        case 0:     // load file
            loadFileButton.show();
            break;
        case 1:     // show file and wait
            drawCircles();
            break;
        case 2:     // show n step
            drawCirclesStep(currentGcode);
            loadBacteriaButton.btext = "Load Bacteria #" + currentGcode;
            loadBacteriaButton.show();
            break;
        case 3:
            drawCirclesStep(currentGcode);
            printButton.btext = "Print Bacteria #"+ currentGcode;
            printButton.show();
            break;

    }
}

// CALLBACKS //////////////////////////

void openCode() {
    File file = null;
    selectInput("Select a SVG file:", "fileSelected", file);
}

void fileSelected(File selection) {
    if (selection == null) {
        println("Window was closed or the user hit cancel.");
    } else {
        println("User selected " + selection.getAbsolutePath());
        svgcode = loadXML(selection.getAbsolutePath());
        fileOpened=true;
        findColors();
        findCircles();
        gcodesGen();
        saveStrings("gcode1.gcode", gcodes.get(0).split("\n"));
        // ok, next step
        processCounter = 1;
    }
}

void mousePressed() {
    if (loadFileButton.overButton() && processCounter == 0) {
        // first step
        openCode();
    }
    if (processCounter == 1) {
        // show the current bacteria
        processCounter++;
        return;
    }

    if (loadBacteriaButton.overButton() && processCounter == 2) {
        loadBacteria();
        processCounter++;
        return;
    }

    if (printButton.overButton() && processCounter == 3) {
        printBacteria();
        return;
    }


}

// get the number of colors in the drawing
void findColors() {
    XML style = svgcode.getChild("style");
    String styles[] = style.getContent().trim().split("\n");
    numberOfColors = styles.length;
    println(numberOfColors);
}

// find all the circles in the drawing and store them
void findCircles() {
    XML[] circles = svgcode.getChildren("g/g/circle");

    for (int i=0; i < circles.length; i++) {
        String cs = circles[i].getString("class");
        float x = circles[i].getFloat("cx");
        float y = circles[i].getFloat("cy");
        float r = circles[i].getFloat("r");

        myCircles.add(new Circle(x,y,r,colorClasses.get(cs)));
    }
}

// draw the circles, full color
void drawCircles() {

    // get viewbox
    String viewBox = svgcode.getString("viewBox");
    int vx, vy, vw, vh;
    vx = int(viewBox.split(" ")[0]);
    vy = int(viewBox.split(" ")[1]);
    vw = int(viewBox.split(" ")[2]);
    vh = int(viewBox.split(" ")[3]);

    // draw the circles mapping the coordinates to the window
    for(int i=0; i< myCircles.size(); i++) {
        Circle c = myCircles.get(i);
        float x,y,r;
        x = map(c.x, vx, vx+vw, 0, WIDTH);
        y = map(c.y, vy, vy+vh, 0, HEIGHT);
        r = c.radius; 
        noStroke();
        fill(c.cl);
        ellipse(round(x),round(y),r,r);
    }
}

// draw the circles, current step highlighted
void drawCirclesStep(int n) {

    // get viewbox
    String viewBox = svgcode.getString("viewBox");
    int vx, vy, vw, vh;
    vx = int(viewBox.split(" ")[0]);
    vy = int(viewBox.split(" ")[1]);
    vw = int(viewBox.split(" ")[2]);
    vh = int(viewBox.split(" ")[3]);

    // draw the circles mapping the coordinates to the window
    for(int i=0; i< myCircles.size(); i++) {
        Circle c = myCircles.get(i);
        float x,y,r;
        x = map(c.x, vx, vx+vw, 0, WIDTH);
        y = map(c.y, vy, vy+vh, 0, HEIGHT);
        r = c.radius; 
        noStroke();
        if (c.cl == colorClasses.valueArray()[n])
            fill(255);
        else
            fill(50);
        ellipse(round(x),round(y),r,r);
    }
}


void gcodesGen() {

    gcodes = new ArrayList<String>();

    // get viewbox
    String viewBox = svgcode.getString("viewBox");
    int vx, vy, vw, vh;
    vx = int(viewBox.split(" ")[0]);
    vy = int(viewBox.split(" ")[1]);
    vw = int(viewBox.split(" ")[2]);
    vh = int(viewBox.split(" ")[3]);

    // for each color
    for (int i = 0; i < numberOfColors; i++) {
        // start
        String gcode = "";
        gcode += header;

        // search circles
        for (int j = 0; j < myCircles.size(); j++) {
            Circle c = myCircles.get(j);

            // if current color
            if (c.cl == colorClasses.valueArray()[i]) {
                // generate g-code for that circle
                float x, y, r;
                // map coordinates
                x = map(c.x, vx, vx+vw, 0, 180);
                y = map(c.y, vy, vy+vh, 0, 180);
                r = c.radius/2;

                if (GO_UP_WHEN_JUMPING)
                    // up
                    gcode += "G1 Z" + GO_UP_DISTANCE + " F5000 \n";

                // move to the center
                // we move to the center, but on X we substract the radius
                // so we start and end the circle there
                gcode += "G1 F" + JUMPSPEED + " X" + Float.toString(x-r) + " Y" + Float.toString(y) + " E1 \n";

                if (GO_UP_WHEN_JUMPING)
                    // down
                    gcode += "G1 Z" + PRINT_HEIGHT + " F5000 \n";

                // now make the circle
                gcode += "M42 P4 S255\n"; // turn on pin P4

                // start and end points are the same
                gcode += "G2 I" + Float.toString(r) 
                    + " J0" + " F" + NORMALSPEED 
                    + " E5 \n";

                if (CONCENTRIC_CIRCLES) {
                  for (float cc = r-1; cc > 0; cc--) {
                    // move to next initial point
                    gcode +=  "G1 F" + JUMPSPEED + " X" + Float.toString(x-cc) + " Y" + Float.toString(y) + " E1 \n";
                    // make the circle
                    gcode += "G2 I" + Float.toString(cc) 
                      + " J0" + " F" + NORMALSPEED 
                      + " E5 \n";
                  }
                }

                gcode += "M42 P4 S0\n"; // turn off P4

            }
        }

        // end
        gcode += footer;

        // add gcode to gcodes list
        gcodes.add(gcode);
    }

}


void loadBacteria() {
}

void printBacteria() {
    // print

    // check if there are more G-Codes to print
    if (currentGcode < gcodes.size()-1) {
        currentGcode++;     // next GCode
        processCounter = 2; // show next bacteria to print
    }
    else {
        currentGcode = 0;
        processCounter = 0;
    }

}

void sendCode() {
  if (gcodeOutput==null) 
    return;

  sending = true;
  sendCodeLine();
}

// sends a gcode line
void sendCodeLine() {
  if (!portOpen) return;
  if (!sending) return;

  // check
  if (currentGcodeLine == gcodeOutput.length) {
    sending = false;
    return;
  }

  // ok, get next line
  String line = gcodeOutput[currentGcodeLine];
  // send it
  println(line);
  port.write(line + "\n");
  
  // next
  currentGcodeLine++;
}

// receives serial events
void serialEvent(Serial p) {
  String s = p.readStringUntil('\n');
  println(s.trim());

  if (s.trim().startsWith("ok"))
    sendCodeLine();

  if (s.trim().startsWith("error"))
      sending = false;
}

